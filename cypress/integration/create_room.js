describe('Create a room, add users and click on generate button', () => {
  const amountOfPeople = 6;
  const amountOfTeams = 2;
  const amountOfPeopleInTeams = 3;

  beforeEach(() => {
    cy.restoreLocalStorage();

    cy.visit('/');

    cy.setLocalStorage('token', 'Bearer when_will_this_all_be_over?');
    cy.setLocalStorage('user', 'Aygulechka@molodec.com');

    cy.visit('/room/'); // go the room page

    cy.wait(7000)

    let input;
    for (let i = 1; i <= amountOfPeople; i++) {
      input = `Test User ${i}`;

      cy.get('.form-control').wait(1000).type(input);

      cy.get('button').contains('add').wait(500).click();
    }
  });

  describe('by defining number of teams', () => {
    beforeEach(() => {
      cy.get('input[type="number"]')
        .clear()
        .type(amountOfTeams.toString())
        .should('have.value', amountOfTeams.toString());
    });

    it('with owner in group', () => {
      cy.get('button').contains('Generate').click();
      cy.get('.MuiList-root')
        .children()
        .should('have.length', amountOfTeams + 1);
      cy.get('.MuiListItemText-root')
        .contains('Aygulechka@molodec.com')
        .should('exist');
    });

    it('without owner in group ', () => {
      cy.get('.PrivateSwitchBase-input-8').click();
      cy.get('button').contains('Generate').click();
      cy.get('.MuiListItemText-root')
        .contains('Aygulechka@molodec.com')
        .should('not.exist');
    });
  });

  describe('by defining number of people in the team', () => {
    beforeEach(() => {
      cy.get('button').contains('Number of people in a team').click();
      cy.get('input[type="number"]')
        .clear()
        .type(amountOfPeopleInTeams.toString())
        .should('have.value', amountOfPeopleInTeams.toString());
    });

    it('with owner in group', () => {
      cy.get('button').contains('Generate').click();
      cy.get('.MuiListItemText-root')
        .contains('Aygulechka@molodec.com')
        .should('exist');
    });

    it('without owner in group ', () => {
      cy.get('.PrivateSwitchBase-input-8').click();
      cy.get('button').contains('Generate').click();
      cy.get('.MuiListItemText-root')
        .contains('Aygulechka@molodec.com')
        .should('not.exist');
    });
  });
});
