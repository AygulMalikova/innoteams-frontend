describe('Click on the `Join the room` button, enter the pin, wait for others, see the results ', () => {
  const roomNumber = '210160';

  beforeEach(() => {
    cy.restoreLocalStorage();

    cy.setLocalStorage('token', 'Bearer when_will_this_all_be_over?');
    cy.setLocalStorage('user', 'Aygulechka@molodec.com');
    cy.visit('/');
  });

  it(' when the room was previously created by another user', () => {
    cy.get('button').contains('Join the group').click();
    cy.get('input[type="number"]')
      .clear()
      .type(roomNumber.toString())
      .should('have.value', roomNumber.toString());
    cy.get('.MuiDialogActions-root > .btn-info').click();

    cy.get('.settings').should('not.exist');
  });
});
