export const room = {
  name: '',
  owner: {
    username: '',
  },
  participants: [],
  pinCode: 0,
  teams: [
    {
      participants: [],
    },
  ],
  uuid: '',
};
