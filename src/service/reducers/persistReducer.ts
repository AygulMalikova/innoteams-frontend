import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import googleTokenReducer from './googleTokenReducer';

const persistConfig = {
  key: 'googleToken',
  storage: storage,
  whitelist: ['googleToken'],
};
const pReducer = persistReducer(persistConfig, googleTokenReducer);
const store = createStore(pReducer);
const persistor = persistStore(store);

export { persistor, store };
