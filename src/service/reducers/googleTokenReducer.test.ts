import googleTokenReducer from './googleTokenReducer';
import { GOOGLE_LOGIN, GOOGLE_LOGOUT } from '../constants/actions';
import faker from 'faker';

describe('Google Token reducer', () => {
  test('return default initial state when no action is passed', () => {
    const newState = googleTokenReducer(undefined, {});
    expect(newState).toEqual({ googleTokenReducer: { tokenID: '', user: '' } });
  });

  test('return new state upon receiving an action of type `GOOGLE_LOGOUT`', () => {
    const newState = googleTokenReducer(undefined, { type: GOOGLE_LOGOUT });
    expect(newState).toEqual({ googleTokenReducer: { tokenID: '', user: '' } });
  });

  test('return new state upon receiving an action of type `GOOGLE_LOGIN`', () => {
    const tokenValue = faker.internet.password;
    const userValue = faker.internet.email;
    const newState = googleTokenReducer(undefined, {
      type: GOOGLE_LOGIN,
      payload: { tokenID: tokenValue, user: userValue },
    });
    expect(newState).toEqual({
      googleTokenReducer: { tokenID: tokenValue, user: userValue },
    });
  });
});
