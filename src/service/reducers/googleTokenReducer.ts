import { combineReducers } from 'redux';
import {
  GOOGLE_LOGIN,
  GOOGLE_LOGOUT,
  GOOGLE_REFRESH,
} from '../constants/actions';

const token = localStorage.getItem('token');
const user = localStorage.getItem('user');
const initialState = { tokenID: token ? token : '', user: user ? user : '' };

const googleTokenReducer = (state = initialState, action) => {
  switch (action.type) {
    case GOOGLE_LOGIN:
      localStorage.setItem('token', action.payload.tokenID);
      localStorage.setItem('user', action.payload.user);
      return {
        ...state,
        tokenID: action.payload.tokenID,
        user: action.payload.user,
      };
    case GOOGLE_LOGOUT:
      localStorage.removeItem('token');
      localStorage.removeItem('user');
      return { ...state, tokenID: '', user: '' };
    case GOOGLE_REFRESH:
      localStorage.removeItem('token');
      localStorage.setItem('token', action.payload.tokenID);
      return { ...state, tokenID: action.payload.tokenID };
    default:
      return state;
  }
};

export default combineReducers({ googleTokenReducer });
