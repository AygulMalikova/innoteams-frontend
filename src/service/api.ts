import axios$, { AxiosRequestConfig } from 'axios';

import { BASE_URL } from './constants/api';
import { store } from './reducers/persistReducer';

const axios = axios$.create();

store.subscribe(() => {
  const select = (state) => {
    return state.googleTokenReducer.tokenID;
  };
  const token = select(store.getState());
  axios.interceptors.request.use((config: AxiosRequestConfig) => {
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  });
});

export const getRoomByUUID = (uuid: string) => {
  return axios.get(`${BASE_URL}/room/${uuid}`);
};

export const addParticipant = (uuid: string, participant: string) => {
  return axios.post(`${BASE_URL}/room/${uuid}/addParticipant`, {
    username: participant,
  });
};

export const distributeParticipants = (uuid: string, roomConfig: any) => {
  return axios.post(
    `${BASE_URL}/room/${uuid}/distributeParticipants`,
    roomConfig
  );
};

export const createRoom = (name: string) => {
  return axios.post(`${BASE_URL}/room/create`, { name });
};

export const enterRoom = (pinCode: number) => {
  return axios.get(`${BASE_URL}/room/enter/${pinCode}`);
};

export const removeUser = (uuid: string, username: string) => {
  return axios.delete(`${BASE_URL}/room/${uuid}/removeParticipant`, {
    data: { username: username },
  });
};
