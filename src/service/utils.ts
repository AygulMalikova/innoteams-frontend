import { store } from './reducers/persistReducer';
import { GOOGLE_REFRESH } from './constants/actions';

export const refreshTokenSetup = (res) => {
  let refreshTiming = (res.tokenObj.expires_in || 3600 - 5 * 60) * 1000;

  const refreshToken = async () => {
    const newAuthRes = await res.reloadAuthResponse();
    refreshTiming = (newAuthRes.tokenObj.expires_in || 3600 - 5 * 60) * 1000;
    store.dispatch({
      type: GOOGLE_REFRESH,
      payload: { tokenID: newAuthRes.tokenId },
    });
    setTimeout(refreshToken, refreshTiming);
  };
  setTimeout(refreshToken, refreshTiming);
};
