import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import {
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  DialogContentText,
  TextField,
} from '@material-ui/core';
import cx from 'classnames';

import styles from './Home.module.scss';

export const Home = () => {
  const [modalOpen, setModalOpen] = React.useState(false);
  const [pinCode, setPinCode] = useState('');
  const [error, setError] = useState('');
  const history = useHistory();

  const openModal = () => {
    setModalOpen(true);
  };

  const closeModal = () => {
    setError('');
    setPinCode('');
    setModalOpen(false);
  };

  const handlePinCodeInput = (event) => {
    setError('');
    const pinCodeFormatValidator = /^[0-9]{0,6}$/;
    if (pinCodeFormatValidator.test(event.target.value)) {
      setPinCode(event.target.value);
    }
  };

  const handleJoinGroup = () => {
    if (pinCode.length !== 6) {
      setError('Invalid pincode format');
      return;
    }
    setModalOpen(false);
    history.push(`/room/${pinCode}`);
  };

  return (
    <>
      <main className={styles['home']} data-testid='home-overlay'>
        <section className={styles['home__links']}>
          <Link to='/room/'>
            <button
              type='button'
              className={cx(styles['home__links_btn'], 'btn btn-info btn-lg')}
            >
              Create new group
            </button>
          </Link>
          <button
            type='button'
            className={cx(styles['home__links_btn'], 'btn btn-info btn-lg')}
            onClick={openModal}
          >
            Join the group
          </button>
        </section>
      </main>
      <Dialog
        className={styles['home__modal']}
        open={modalOpen}
        onClose={closeModal}
        aria-labelledby='form-join-group-title'
      >
        <DialogTitle id='form-join-group-title'>Join the group</DialogTitle>
        <DialogContent>
          <DialogContentText>Enter 6-digit pin code</DialogContentText>
          <TextField
            className={styles['home__modal_input']}
            error={error.length > 0}
            aria-errormessage={error}
            label='Pincode'
            type='number'
            variant='filled'
            onInput={handlePinCodeInput}
            value={pinCode}
            data-testid='input-pin'
          />
        </DialogContent>
        <DialogActions className={styles['home__links_modal']}>
          <button
            className={cx(styles['home__links_btn'], 'btn btn-outline-info')}
            onClick={closeModal}
          >
            Cancel
          </button>
          <button
            className={cx(styles['home__links_btn'], 'btn btn-info')}
            onClick={handleJoinGroup}
            data-testid='join-btn'
          >
            Join
          </button>
        </DialogActions>
      </Dialog>
    </>
  );
};
