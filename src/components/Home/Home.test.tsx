import { render, screen, fireEvent } from '@testing-library/react';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Home } from './Home';

const renderHome = () => {
  return render(
    <BrowserRouter>
      <Home />
    </BrowserRouter>
  );
};

/**
 * Testing the functionality of the Home page
 */
describe('Home page works correctly:', () => {
  const mockSetOpen = jest.fn();
  beforeEach(() => {
    mockSetOpen.mockClear();
  });
  test('opens modal correctly', () => {
    renderHome();
    const joinGroupButton = screen.getByText(/Join the group/i);

    fireEvent.click(joinGroupButton);
    expect(screen.getByText('Enter 6-digit pin code')).toBeInTheDocument();
  });
  describe('validated pin code correctly', () => {
    let submitBtn: HTMLElement;
    let inputEl: HTMLInputElement;
    beforeEach(() => {
      React.useState = jest.fn(() => [true, mockSetOpen]);

      renderHome();
      submitBtn = screen.getByTestId('join-btn');
      inputEl = screen.getByTestId('input-pin').children[1]
        .children[0] as HTMLInputElement;
    });
    test('when input is incorrect', async () => {
      fireEvent.input(inputEl, { target: { value: '123' } });
      fireEvent.click(submitBtn);

      expect(inputEl).toHaveAttribute('aria-invalid', 'true');
      mockSetOpen.mockClear();
    });
    test('when input is correct', () => {
      fireEvent.input(inputEl, { target: { value: '123456' } });
      fireEvent.click(submitBtn);

      expect(inputEl).toHaveAttribute('aria-invalid', 'false');
      mockSetOpen.mockClear();
    });
  });
});
