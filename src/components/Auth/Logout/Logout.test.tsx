import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { Logout } from './Logout';
import { store } from '../../../service/reducers/persistReducer';

/**
 * Testing the functionality of the Logout component
 */
describe('Lotout page', () => {
  const mockSetOpenError = jest.fn();
  beforeEach(() => {
    mockSetOpenError.mockClear();
  });

  /**
   * Function for rendering lotout component inside Provider and BrowserRouter
   */
  const renderAppComponent = () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Logout />
        </BrowserRouter>
      </Provider>
    );
  };

  test('displays alert on error', () => {
    React.useState = jest.fn(() => [true, mockSetOpenError]);

    renderAppComponent();

    expect(screen.getByText(/Logout failed!/i)).toBeInTheDocument();
  });

  test('hides alert on close button click', () => {
    React.useState = jest.fn(() => [true, mockSetOpenError]);

    renderAppComponent();
    const closeButton = screen.getByLabelText('Close');
    fireEvent.click(closeButton);

    expect(mockSetOpenError).toHaveBeenCalledWith(false);
  });
});
