import React from 'react';
import { GoogleLogout } from 'react-google-login';
import { useDispatch } from 'react-redux';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { clientId } from '../../../service/constants/google';
import { GOOGLE_LOGOUT } from '../../../service/constants/actions';

export const Logout = () => {
  const Alert = (props) => {
    return <MuiAlert elevation={6} variant='filled' {...props} />;
  };

  const dispatch = useDispatch();
  const [openError, setOpenError] = React.useState(false);

  const onSuccess = () => {
    dispatch({ type: GOOGLE_LOGOUT });
  };

  const onFailure = () => {
    setOpenError(true);
  };

  const handleClose = () => {
    setOpenError(false);
  };

  return (
    <div>
      <Snackbar
        open={openError}
        autoHideDuration={3000}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      >
        <Alert onClose={handleClose} severity='error'>
          Logout failed!
        </Alert>
      </Snackbar>
      <GoogleLogout
        clientId={clientId}
        buttonText='Logout'
        onLogoutSuccess={onSuccess}
        onFailure={onFailure}
      />
    </div>
  );
};
