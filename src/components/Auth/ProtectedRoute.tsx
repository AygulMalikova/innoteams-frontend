import React from 'react';
import { Route, Redirect } from 'react-router-dom';

// eslint-disable-next-line react/prop-types
const ProtectedRoute = ({ component: Component, ...props }) => {
  return (
    <Route exact>
      {() =>
        // eslint-disable-next-line react/prop-types
        props.token ? <Component {...props} /> : <Redirect to='/login' />
      }
    </Route>
  );
};

export default ProtectedRoute;
