import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { store } from '../../../service/reducers/persistReducer';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { Login } from './Login';

/**
 * Testing the functionality of the Login component
 */
describe('Login page', () => {
  const mockSetOpenError = jest.fn();
  beforeEach(() => {
    mockSetOpenError.mockClear();
  });

  /**
   * Function for rendering login component inside Provider and BrowserRouter
   */
  const renderLoginComponent = () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <Login />
        </BrowserRouter>
      </Provider>
    );
  };

  test('displays alert on error', () => {
    React.useState = jest.fn(() => [true, mockSetOpenError]);

    renderLoginComponent();

    expect(screen.getByText(/Login failed!/i)).toBeInTheDocument();
  });

  test('hides alert on close button click', () => {
    React.useState = jest.fn(() => [true, mockSetOpenError]);

    renderLoginComponent();
    const closeButton = screen.getByLabelText('Close');
    fireEvent.click(closeButton);

    expect(mockSetOpenError).toHaveBeenCalledWith(false);
  });
});
