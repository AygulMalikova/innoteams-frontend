import React from 'react';
import { useDispatch } from 'react-redux';
import { GoogleLogin } from 'react-google-login';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { useHistory } from 'react-router-dom';

import { clientId } from '../../../service/constants/google';
import { GOOGLE_LOGIN } from '../../../service/constants/actions';
import styles from './Login.module.scss';
import { refreshTokenSetup } from '../../../service/utils';

export const Login = () => {
  const history = useHistory();

  const Alert = (props) => {
    return <MuiAlert elevation={6} variant='filled' {...props} />;
  };

  const dispatch = useDispatch();
  const [openError, setOpenError] = React.useState(false);

  const onSuccess = (res) => {
    dispatch({
      type: GOOGLE_LOGIN,
      payload: { tokenID: res.tokenId, user: res.profileObj.email },
    });
    refreshTokenSetup(res);
    history.push('/');
  };

  const onFailure = () => {
    setOpenError(true);
  };

  const handleClose = () => {
    setOpenError(false);
  };

  return (
    <>
      <Snackbar
        open={openError}
        autoHideDuration={3000}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
      >
        <Alert onClose={handleClose} severity='error'>
          Login failed!
        </Alert>
      </Snackbar>
      <GoogleLogin
        className={styles['login']}
        clientId={clientId}
        buttonText='Sign up with a Google Account'
        onSuccess={onSuccess}
        onFailure={onFailure}
        isSignedIn={true}
      />
    </>
  );
};
