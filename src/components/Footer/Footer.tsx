import React from 'react';

import styles from './Footer.module.scss';

export const Footer = () => {
  return (
    <footer className={styles['footer']}>
      <div className='container'>
        <span className='text-muted'>© 2021 ADADA Team</span>
      </div>
    </footer>
  );
};
