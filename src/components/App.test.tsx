import React from 'react';
import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from '../service/reducers/persistReducer';
import { BrowserRouter } from 'react-router-dom';
import App from './App';

/**
 * Testing the functionality of the App component
 */
describe('App component', () => {
  test('renders correctly', () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </Provider>
    );
    expect(screen.getByTestId('app-component')).toBeInTheDocument();
  });
});
