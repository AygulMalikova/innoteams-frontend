import React from 'react';
import { Room } from './Room';
import { render, screen } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import * as reactRedux from 'react-redux';
import { store } from '../../service/reducers/persistReducer';
import faker from 'faker';

const renderRoom = () => {
  render(
    <reactRedux.Provider store={store}>
      <BrowserRouter>
        <Room />
      </BrowserRouter>
    </reactRedux.Provider>
  );
};
/**
 * Testing the functionality of the Room component
 */
describe('Room page', () => {
  const useSelectorMock = jest.spyOn(reactRedux, 'useSelector'); // mock useSelector
  const mockSetLoading = jest.fn();
  const mockSetUsers = jest.fn();

  beforeEach(() => {
    useSelectorMock.mockClear(); //clear mocks
    mockSetLoading.mockClear();
  });

  test('renders loading on component mount', () => {
    renderRoom();
    expect(screen.getByRole('progressbar')).toBeInTheDocument();
  });

  test('renders content when page is loaded', () => {
    React.useState = jest.fn(() => [false, mockSetLoading]);
    useSelectorMock.mockReturnValue({
      tokenId: faker.internet.password,
      user: faker.internet.email,
    });
    React.useState = jest.fn(() => [[], mockSetUsers]);

    renderRoom();

    expect(screen.queryByTestId('progressbar ')).not.toBeInTheDocument();
  });
});
