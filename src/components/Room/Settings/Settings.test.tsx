import { render, screen, fireEvent } from '@testing-library/react';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Settings } from './Settings';

const handleSettings = jest.fn();

const renderSettings = () => {
  return render(
    <BrowserRouter>
      <Settings handleSettings={handleSettings} />
    </BrowserRouter>
  );
};

/**
 * Testing the functionality of the settings component
 */
describe('Settings component works correctly:', () => {
  test('renders correctly', () => {
    renderSettings();

    expect(screen.getByTestId('settings-component')).toBeInTheDocument();
  });

  test('changes tab correctly', () => {
    renderSettings();
    const tabs = screen.getAllByRole('tab');

    fireEvent.click(tabs[1]);

    expect(tabs[1]).toHaveAttribute('aria-selected', 'true');
    expect(handleSettings).toHaveBeenCalledWith({
      mode: 1,
      amount: 0,
      includeOwner: true,
    });
  });

  test('changes input correctly', () => {
    const { container } = renderSettings();

    const input = container.querySelector('input') as HTMLInputElement;

    fireEvent.input(input, { target: { value: '2' } });

    expect(handleSettings).toHaveBeenCalledWith({
      mode: 0,
      amount: '2',
      includeOwner: true,
    });
  });

  test('changes toggle correctly', () => {
    const { container } = renderSettings();

    const toggler = container.querySelectorAll('input')[1] as HTMLInputElement;

    fireEvent.click(toggler);

    expect(handleSettings).toHaveBeenCalledWith({
      mode: 0,
      amount: 0,
      includeOwner: false,
    });
  });
});
