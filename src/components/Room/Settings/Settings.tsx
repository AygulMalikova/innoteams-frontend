import React, { useState } from 'react';
import {
  AppBar,
  Tab,
  Tabs,
  TextField,
  FormControlLabel,
  Switch,
} from '@material-ui/core';

import styles from './Settings.module.scss';

export const Settings = ({ handleSettings }) => {
  const [settings, setSettings] = useState({
    mode: 0,
    amount: 0,
    includeOwner: true,
  });
  const [checked, setChecked] = useState(true);

  const handleChange = (event, newChoice) => {
    setSettings({ ...settings, mode: newChoice });
    handleSettings({ ...settings, mode: newChoice });
  };

  const handleInput = (event) => {
    setSettings({ ...settings, amount: event.target.value });
    handleSettings({ ...settings, amount: event.target.value });
  };

  const toggleChecked = (event) => {
    setChecked(event.target.checked);
    handleSettings({ ...settings, includeOwner: event.target.checked });
  };

  return (
    <div className={styles['settings']} data-testid='settings-component'>
      <AppBar position='static' className={styles['settings__tabs']}>
        <Tabs
          value={settings.mode}
          onChange={handleChange}
          centered={true}
          indicatorColor='primary'
        >
          <Tab label='Number of teams' />
          <Tab label='Number of people in a team' />
        </Tabs>
      </AppBar>
      <form>
        <TextField
          className={styles['settings__input']}
          required
          label={
            settings.mode === 0
              ? 'Number of teams'
              : 'Number of people in a team'
          }
          type='number'
          onInput={handleInput}
          value={settings.amount}
        />
        <FormControlLabel
          control={
            <Switch
              checked={checked}
              onChange={toggleChecked}
              color='primary'
            />
          }
          label='Include Owner in Group'
          className='mt-3'
        />
      </form>
    </div>
  );
};
