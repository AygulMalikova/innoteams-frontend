import React from 'react';
import {
  render,
  screen,
  act,
  fireEvent,
  waitFor,
} from '@testing-library/react';
import { Users } from './Users';
import faker from 'faker';
import * as reactRedux from 'react-redux';
import { store } from '../../../service/reducers/persistReducer';
import { BrowserRouter } from 'react-router-dom';
import { room } from '../../../service/initialStates/room';

const addUserFormId = 'add-user-form';
const addUserFormInputId = 'add-user-form-input';
const userItemId = 'user-item';
const removeUserBtnId = 'remove-user-btn';
const cancelUserBtnId = 'cancel-user-btn';

/**
 * Testing the functionality of the Users component
 */
describe('Users list works correctly:', () => {
  describe('adds news users', () => {
    let inputComponent: HTMLInputElement;
    beforeEach(() => {
      render(
        <reactRedux.Provider store={store}>
          <BrowserRouter>
            <Users room={room} />
          </BrowserRouter>
        </reactRedux.Provider>
      );
      expect(screen.queryAllByTestId(userItemId)).toHaveLength(0); //initially the list of users is empty

      inputComponent = screen.getByTestId(
        addUserFormInputId
      ) as HTMLInputElement;

      // typing some random email in the input field
      act(() => {
        fireEvent.input(inputComponent, {
          target: { value: faker.internet.email() },
        });
      });
    });

    test('and submits form', () => {
      const addUserForm = screen.getByTestId(addUserFormId);
      // typing some random email in the input field
      act(() => {
        fireEvent.input(inputComponent, {
          target: { value: faker.internet.email() },
        });
      });

      // submitting the form
      act(() => {
        fireEvent.click(addUserForm);
      });
    });
    test('and clicks on cancel', () => {
      const cancelUserBtn = screen.getByTestId(cancelUserBtnId);
      fireEvent.click(cancelUserBtn);
      expect(inputComponent).toHaveValue('');
    });
  });

  test('removes user correctly', async () => {
    const mockSetUsers = jest.fn();

    React.useState = jest.fn(() => [['test', 'test2'], mockSetUsers]);
    render(
      <reactRedux.Provider store={store}>
        <BrowserRouter>
          <Users room={room} />
        </BrowserRouter>
      </reactRedux.Provider>
    );
    const removeUserBtn = screen.queryAllByTestId(removeUserBtnId)[0]; //get first remove button in the list

    //clicking on the remove button
    act(() => {
      fireEvent.click(removeUserBtn);
    });

    await waitFor(() =>
      expect(screen.queryAllByTestId(userItemId)).toHaveLength(2)
    ); //list of users decremented by one
    mockSetUsers.mockClear();
  });

  test('render list with existing users', () => {
    const mockSetUsers = jest.fn();

    React.useState = jest.fn(() => [['test', 'test2'], mockSetUsers]);
    render(
      <reactRedux.Provider store={store}>
        <BrowserRouter>
          <Users room={room} />
        </BrowserRouter>
      </reactRedux.Provider>
    );
    expect(screen.queryAllByTestId(userItemId)).toHaveLength(2); //initially the list of users is set with 2 users

    mockSetUsers.mockClear();
  });
});
