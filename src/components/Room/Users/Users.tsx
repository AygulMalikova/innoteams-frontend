import styles from './Users.module.scss';
import React, { FormEvent, useState } from 'react';
import { useSelector } from 'react-redux';
import cx from 'classnames';
import remove from '../../../assets/trash.svg';
import { addParticipant, removeUser } from '../../../service/api';

export const Users = (props) => {
  const [users, setUsers] = React.useState(
    Array.isArray(props.room.participants) ? props.room.participants : []
  );
  const currentUser = useSelector((state) => state.googleTokenReducer.user);
  const isOwner = currentUser === props.room.owner.username;
  const [newUser, setNewUser] = useState('');

  function handleSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    addParticipant(props.room.uuid, newUser)
      .then(() => {
        setUsers([newUser, ...users]);
        setNewUser('');
      })
      .catch((err) => console.log(err));
  }

  function handleCancel() {
    setNewUser('');
  }

  function removeItem(i: number) {
    removeUser(props.room.uuid, users[i])
      .then(() => {
        setUsers([...users.slice(0, i), ...users.slice(i + 1)]);
      })
      .catch((err) => console.log(err));
  }

  return (
    <section className={cx(styles['room__users'], 'container')}>
      <h2 className='room__users_header'>Users in the room</h2>

      {isOwner && (
        <form onSubmit={handleSubmit} data-testid='add-user-form'>
          <div className='form-group'>
            <label htmlFor='text'>Add user</label>
            <div className='input-group mb-2'>
              <input
                className='form-control'
                value={newUser}
                onChange={(e) => setNewUser(e.target.value)}
                data-testid='add-user-form-input'
              />
              <div className='input-group-append'>
                <button className='btn btn-outline-info' type='submit'>
                  add
                </button>
                <button
                  className='btn btn-outline-secondary'
                  type='button'
                  onClick={handleCancel}
                  data-testid='cancel-user-btn'
                >
                  cancel
                </button>
              </div>
            </div>
          </div>
        </form>
      )}

      <ul className={cx(styles['room__users_list'], 'list-group', 'shadow')}>
        {users.map((name, i) => {
          return (
            <li
              key={i}
              className={cx(
                styles['room__users_list__item'],
                'list-group-item'
              )}
              data-testid='user-item'
            >
              {name.username}
              {isOwner && (
                <div className='float-right'>
                  <img
                    className={styles['room__users_list__item_img']}
                    onClick={() => removeItem(i)}
                    src={remove}
                    alt='remove'
                    data-testid='remove-user-btn'
                  />
                </div>
              )}
            </li>
          );
        })}
      </ul>

      {users.length === 0 && (
        <p className='text-center text-muted'>No user in the room</p>
      )}
    </section>
  );
};
