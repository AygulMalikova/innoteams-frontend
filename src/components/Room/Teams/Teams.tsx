import React from 'react';
import { List, ListItem, ListItemText, ListSubheader } from '@material-ui/core';
import styles from './Teams.module.scss';
import cx from 'classnames';

export const Teams = (props) => {
  return (
    <List
      className={styles['teams__list']}
      subheader={<li />}
      data-testid='teams-component'
    >
      {props.teams.map((team, teamId) => (
        <li
          key={`section-${teamId}`}
          className={cx(styles['teams__list_section'], 'shadow')}
        >
          <ul className={cx(styles['teams__list_ul'])}>
            <ListSubheader
              className={cx(styles['teams__list__title'])}
            >{`Team: ${teamId}`}</ListSubheader>
            {team.participants.map((person, personId) => (
              <ListItem key={`item-${teamId}-${personId}`}>
                <ListItemText primary={person} />
              </ListItem>
            ))}
          </ul>
        </li>
      ))}
    </List>
  );
};
