import { render, screen } from '@testing-library/react';
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Teams } from './Teams';

const teams = [
  { participants: ['test1', 'test2'] },
  { participants: ['test3', 'test4'] },
  { participants: ['test5', 'test6'] },
];

const renderTeams = () => {
  return render(
    <BrowserRouter>
      <Teams teams={teams} />
    </BrowserRouter>
  );
};

/**
 * Testing the functionality of the teams component
 */
describe('Teams component works correctly:', () => {
  test('renders correctly', () => {
    renderTeams();

    expect(screen.getByTestId('teams-component')).toBeInTheDocument();
  });
});
