import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import styles from './Room.module.scss';
import { Users } from './Users/Users';

import { room as roomInitialState } from '../../service/initialStates/room';
import { CircularProgress } from '@material-ui/core';
import {
  createRoom,
  distributeParticipants,
  enterRoom,
  getRoomByUUID,
} from '../../service/api';
import { Settings } from './Settings/Settings';
import cx from 'classnames';
import { Teams } from './Teams/Teams';
import { useHistory } from 'react-router-dom';

export const Room = () => {
  const [loading, setLoading] = React.useState(true);
  const [room, setRoomInfo] = useState(roomInitialState);
  const [settings, setSettings] = useState({
    mode: 0,
    amount: 0,
    includeOwner: true,
  });
  const [isOwner, setIsOwner] = useState(false);

  const user = useSelector((state) => state.googleTokenReducer.user);
  const history = useHistory();

  const generateTeams = () => {
    distributeParticipants(room.uuid, {
      distributionConfiguration: settings.amount,
      includeOwner: settings.includeOwner,
      mode: settings.mode === 0 ? 'NUMBER_OF_TEAMS' : 'NUMBER_OF_PARTICIPANTS',
    }).then((res: any) => {
      setRoomInfo({ ...room, teams: res.data.teams });
    });
  };

  const handleSettings = (newSettings) => {
    setSettings(newSettings);
  };

  React.useEffect(() => {
    setLoading(true);
    const currentRoom = window.location.pathname.replace('/room/', '');
    if (currentRoom && parseInt(currentRoom) && currentRoom.length === 6) {
      enterRoom(parseInt(currentRoom)).then((res: any) => {
        setRoomInfo(res.data);
        setLoading(false);
        setIsOwner(user === res.data.owner.username);
        history.push(`/room/${res.data.uuid}`);
      });
    } else if (currentRoom.length === 0) {
      createRoom('').then((res: any) => {
        setRoomInfo(res.data);
        setLoading(false);
        setIsOwner(user === res.data.owner.username);
        history.push(`/room/${res.data.uuid}`);
      });
    } else {
      getRoomByUUID(currentRoom).then((res: any) => {
        setRoomInfo(res.data);
        setLoading(false);
        setIsOwner(user === res.data.owner.username);
      });
    }
  }, []);

  React.useEffect(() => {
    const interval = setInterval(() => {
      if (room.uuid) {
        getRoomByUUID(room.uuid).then((res: any) => {
          setRoomInfo(res.data);
        });
      }
    }, 5000);
    return () => clearInterval(interval);
  }, [room]);

  return (
    <>
      {loading ? (
        <CircularProgress className={styles['room__loading']} />
      ) : (
        <main className={styles['room']} data-testid='room-component'>
          <section className={styles['room__number']}>
            <div className={styles['room__number_value']}>{room.pinCode}</div>
          </section>
          <Users room={room} />
          {isOwner && (
            <>
              <Settings handleSettings={handleSettings} />
              <button
                type='button'
                onClick={generateTeams}
                className={cx(styles['room__btn'], 'btn btn-info btn-lg')}
              >
                Generate
              </button>
            </>
          )}
          <Teams teams={room.teams} />
        </main>
      )}
    </>
  );
};
