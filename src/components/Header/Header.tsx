import React from 'react';
import { useSelector } from 'react-redux';
import logoPath from '../../assets/IU_icon.png';

import styles from './Header.module.scss';
import cx from 'classnames';
import { Logout } from '../Auth/Logout/Logout';

export const Header = () => {
  const token = useSelector((state) => state.googleTokenReducer.tokenID);

  return (
    <div className={styles['header']}>
      <nav className='navbar navbar-light bg-light'>
        <div className='container container-fluid'>
          <a className={cx(styles['header__logo'], 'navbar-brand')} href='/'>
            <img
              src={logoPath}
              alt='Innopolis University'
              width='50'
              height='50'
            />
            <p>InnoTeams</p>
          </a>
          {token && <Logout />}
        </div>
      </nav>
    </div>
  );
};
