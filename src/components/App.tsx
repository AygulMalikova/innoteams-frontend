import React from 'react';
import { Header } from './Header/Header';
import { Home } from './Home/Home';
import { Footer } from './Footer/Footer';
import { Redirect, Route, Switch } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { Room } from './Room/Room';
import bgImage from '../assets/bg.jpg';
import { Login } from './Auth/Login/Login';
import ProtectedRoute from './Auth/ProtectedRoute';

function App() {
  const token = useSelector((state) => state.googleTokenReducer.tokenID);

  return (
    <div className='page__content' data-testid='app-component'>
      <Header />
      <div className='page__section'>
        <Switch>
          <ProtectedRoute exact path='/' component={Home} token={token} />
          <ProtectedRoute
            exact
            path='/room/:pinCode?'
            component={Room}
            token={token}
          />
          <Route path='/login'>
            <Login />
          </Route>

          <Route path='/'>
            <Redirect to='/' />
          </Route>
        </Switch>
      </div>
      <Footer />
      <img src={bgImage} alt='Innopolis University' className='page__bg' />
    </div>
  );
}

export default App;
