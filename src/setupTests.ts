// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom/extend-expect';
import { cleanup } from '@testing-library/react';

let origErrorConsole: typeof console.error;

/**
 * Unmount React trees that were mounted with render after each test
 */
afterEach(() => {
  cleanup();
  window.console.error = origErrorConsole;
});
