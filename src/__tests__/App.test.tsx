import React from 'react';
import { render, screen } from '@testing-library/react';
import App from '../components/App';
import * as reactRedux from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import faker from 'faker';
import { store } from '../service/reducers/persistReducer';

/**
 * Testing the functionality of the App component with different configurations
 */
describe('App component renders component correctly', () => {
  const useSelectorMock = jest.spyOn(reactRedux, 'useSelector'); // mock useSelector

  /**
   * Function for rendering app component inside Provider and BrowserRouter
   */
  const renderAppComponent = () => {
    render(
      <reactRedux.Provider store={store}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </reactRedux.Provider>
    );
  };

  beforeEach(() => {
    useSelectorMock.mockClear(); //clear mocks
  });

  describe('renders main content correctly', () => {
    /**
     * When user is authorized the home page with two buttons have to be displayed
     */
    test('when user is authorized', () => {
      useSelectorMock.mockReturnValue({
        tokenId: faker.internet.password,
        user: faker.internet.email,
      });
      renderAppComponent();

      expect(screen.getByText(/Create new group/i)).toBeInTheDocument();
      expect(screen.getByText(/Join the group/i)).toBeInTheDocument();
    });

    /**
     * When user isn't authorized the link to sign up with a Google Account have to be displayed
     */
    test('when user is not authorized', () => {
      useSelectorMock.mockReturnValue(null);
      renderAppComponent();

      expect(
        screen.getByText(/Sign up with a Google Account/i)
      ).toBeInTheDocument();
    });
  });

  describe('renders Header component correctly', () => {
    /**
     * When user is authorized the header with Logout button have to be displayed
     */
    test('when user is authorized', () => {
      useSelectorMock.mockReturnValue({
        tokenId: faker.internet.password,
        user: faker.internet.email,
      });
      renderAppComponent();

      expect(screen.queryByText(/Logout/i)).toBeInTheDocument();
    });

    /**
     * When user isn't authorized the header is empty
     */
    test('user is not authorized', () => {
      useSelectorMock.mockReturnValue(null);
      renderAppComponent();

      expect(screen.queryByText(/Logout/i)).toBeNull();
    });
  });

  describe('renders footer correctly', () => {
    test('in any case', () => {
      renderAppComponent();

      expect(screen.getByText(/2021 ADADA Team/i)).toBeInTheDocument();
    });
  });
});
